export type Team = {
    name: string;
    color: string;
}

export const teams: Team[] = [
    { name: "blublublbublublbll", color: "#5b99c7" },
    { name: "FEUR 🏒", color: "#ed465b" },
    { name: "Diving Vector", color: "#fe8325" },
    { name: "Lila Legends", color: "#5b4886" },
];