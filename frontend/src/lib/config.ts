// Reads /config.json and store it locally in a variable to redistribute it to the app, we're using VueJS
import config from "../config.json";

// Create type for config
export type Config = {
    api: string;
};

// Create variable to store config
let c: Config | null = null;

export const api = () => {
    if (c == null) {
        throw new Error("Config not loaded");
    }
    return c.api;
};

export const loadConfig = () => {
    return new Promise((resolve) => {
        if (c != null) {
            resolve(c);
        }
    
        // Do not render on server
        if (typeof window === "undefined") {
            return null;
        }

        c = config
        resolve(c);
    });
};
