package web

import (
	"encoding/json"
	"poc-helloasso/autogen"
	"poc-helloasso/internal/api"
	"poc-helloasso/internal/config"

	"github.com/gin-gonic/gin"
)

func OptionalString(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}

func Setup(g *gin.Engine) {
	conf := config.GetConfig()

	g.Static("/", "./www")

	g.NoRoute(func(c *gin.Context) {
		// If on base URL, redirect to index.html
		if c.Request.URL.Path == "/" {
			c.File("./www/index.html")
			return
		} else {
			c.Redirect(302, conf.BaseURL)
			return
		}
	})

	type CheckoutForm struct {
		Amount    int32  `json:"amount"`
		Streamer  string `json:"streamer"`
		Team      string `json:"team"` // Bastien
		Anonymous bool   `json:"anonymous"`
	}

	type Redirect struct {
		RedirectURL string `json:"redirectUrl"`
	}

	g.POST("/", func(c *gin.Context) {
		var form CheckoutForm
		if err := c.ShouldBindJSON(&form); err != nil {
			c.JSON(400, gin.H{
				"error": err.Error(),
			})
			return
		}

		token, err := api.Client.GetToken()
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		metadata := map[string]interface{}{
			// "streamer": form.Streamer,
			// "team":     form.Team,
		}

		if form.Streamer != "" && form.Streamer != "-1" {
			metadata["streamer"] = form.Streamer
		}

		if form.Team != "" && form.Team != "-1" {
			metadata["team"] = form.Team
		}

		if form.Anonymous {
			metadata["anonymous"] = true
		}

		resp, err := api.Client.OrganizationCheckoutIntentsPostInitCheckout(
			c,
			conf.HelloAssoConfig.Slug,
			&autogen.OrganizationCheckoutIntentsPostInitCheckoutParams{
				Authorization: OptionalString(token.AccessToken),
			},
			autogen.HelloAssoApiV5ModelsCartsInitCheckoutBody{
				BackUrl:           conf.BaseURL,
				ErrorUrl:          conf.BaseURL + "error.html",
				ReturnUrl:         conf.BaseURL + "thanks.html",
				InitialAmount:     form.Amount,
				TotalAmount:       form.Amount,
				ContainsDonation:  true,
				ItemName:          "Donation",
				Metadata:          &metadata,
				TrackingParameter: OptionalString("101"),
			},
		)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		var bdy Redirect

		// unmarshal resp.Body into bdy
		if err := json.NewDecoder(resp.Body).Decode(&bdy); err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		c.JSON(200, bdy)
	})
}
