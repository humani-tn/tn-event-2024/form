# Build go app
FROM golang AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main cmd/server/main.go

# Build frontend
FROM node:alpine AS frontend

WORKDIR /app

COPY frontend/package.json .
COPY frontend/package-lock.json .

RUN npm install

COPY frontend .

RUN npm run build

# Build final image
FROM scratch

WORKDIR /app

COPY --from=builder /app/main .
COPY --from=frontend /app/build www

CMD ["./main"]
